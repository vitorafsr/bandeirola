/*
  Copyright (c) 2017 Vitor Augusto <vitorafsr@gmail.com>
	Adapted to bandeirola project.

  Copyright (c) Somebody

  One MAX7219 connected to an 8x8 LED matrix.
 */
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include "max7219led8x8.h"
#include <avr/io.h>
#include <util/delay.h>

#define CLK_HIGH()  PORTB |= (1<<PB2)
#define CLK_LOW()   PORTB &= ~(1<<PB2)
#define CS_HIGH()   PORTB |= (1<<PB1)
#define CS_LOW()    PORTB &= ~(1<<PB1)
#define DATA_HIGH() PORTB |= (1<<PB0)
#define DATA_LOW()  PORTB &= ~(1<<PB0)
#define INIT_PORT() DDRB |= ((1<<PB0) | (1<<PB1) | (1<<PB2))

const __flash uint8_t smile[8] = {
        0b00000000,
        0b01100110,
        0b01100110,
        0b00011000,
        0b00011000,
        0b10000001,
        0b01000010,
        0b00111100};

const __flash uint8_t sad[8] = {
        0b00000000,
        0b01100110,
        0b01100110,
        0b00011000,
        0b00011000,
        0b00000000,
        0b00111100,
        0b01000010,
};

const __flash uint8_t temp[3] = {
        0b11100000,
        0b01000000,
        0b01000000,
};

const __flash uint8_t humid[3] = {
        0b10100000,
        0b10100000,
        0b11100000,
};

const __flash uint8_t zero[5] = {
        0b1111,
        0b1001,
        0b1001,
        0b1001,
        0b1111,
};

const __flash uint8_t one[5] = {
        0b0010,
        0b0110,
        0b1010,
        0b0010,
        0b0010,
};

const __flash uint8_t two[5] = {
        0b1111,
        0b0001,
        0b1111,
        0b1000,
        0b1111,
};

const __flash uint8_t three[5] = {
        0b1111,
        0b0001,
        0b1111,
        0b0001,
        0b1111,
};

const __flash uint8_t four[5] = {
        0b1001,
        0b1001,
        0b1111,
        0b0001,
        0b0001,
};

const __flash uint8_t five[5] = {
        0b1111,
        0b1000,
        0b1111,
        0b0001,
        0b1111,
};

const __flash uint8_t six[5] = {
        0b1000,
        0b1000,
        0b1111,
        0b1001,
        0b1111,
};

const __flash uint8_t seven[5] = {
        0b1111,
        0b0001,
        0b0001,
        0b0001,
        0b0001,
};

const __flash uint8_t eight[5] = {
        0b1111,
        0b1001,
        0b1111,
        0b1001,
        0b1111,
};

const __flash uint8_t nine[5] = {
        0b1111,
        0b1001,
        0b1111,
        0b0001,
        0b0001,
};


void spi_send(uint8_t data)
{
    uint8_t i;

    for (i = 0; i < 8; i++, data <<= 1)
    {
	CLK_LOW();
	if (data & 0x80)
	    DATA_HIGH();
	else
	    DATA_LOW();
	CLK_HIGH();
    }
    
}

void max7219_writec(uint8_t high_byte, uint8_t low_byte)
{
    CS_LOW();
    spi_send(high_byte);
    spi_send(low_byte);
    CS_HIGH();
}

void max7219_clear(void)
{
    uint8_t i;
    for (i = 0; i < 8; i++)
    {
	max7219_writec(i+1, 0);
    }
}

void max7219_init(void)
{
    INIT_PORT();
    // Decode mode: none
    max7219_writec(0x09, 0);
    // Intensity: 3 (0-15)
    max7219_writec(0x0A, 1);
    // Scan limit: All "digits" (rows) on
    max7219_writec(0x0B, 7);
    // Shutdown register: Display on
    max7219_writec(0x0C, 1);
    // Display test: off
    max7219_writec(0x0F, 0);
    max7219_clear();
}


uint8_t display[8];

void update_display(void)
{
    uint8_t i;

    for (i = 0; i < 8; i++)
    {
	max7219_writec(i+1, display[i]);
    }
}

void image(const __flash uint8_t im[8])
{
    uint8_t i;

    for (i = 0; i < 8; i++)
	display[i] = im[i];
}

void image2(unsigned char h, unsigned char dig1, unsigned char dig2)
{
	uint8_t i;
	if (h == 't')
		for (i=0; i<3; i++)
			display[i] = temp[i];
	else
		for (i=0; i<3; i++)
			display[i] = humid[i];
		
	const __flash uint8_t * pdig1;
	const __flash uint8_t * pdig2;

	switch(dig1)
	{
		case 0: pdig1 = zero; break;
		case 1: pdig1 = one; break;
		case 2: pdig1 = two; break;
		case 3: pdig1 = three; break;
		case 4: pdig1 = four; break;
		case 5: pdig1 = five; break;
		case 6: pdig1 = six; break;
		case 7: pdig1 = seven; break;
		case 8: pdig1 = eight; break;
		case 9: pdig1 = nine; break;
	}
	switch(dig2)
	{
		case 0: pdig2 = zero; break;
		case 1: pdig2 = one; break;
		case 2: pdig2 = two; break;
		case 3: pdig2 = three; break;
		case 4: pdig2 = four; break;
		case 5: pdig2 = five; break;
		case 6: pdig2 = six; break;
		case 7: pdig2 = seven; break;
		case 8: pdig2 = eight; break;
		case 9: pdig2 = nine; break;
	}


	for (i=0; i<5; i++)
		display[3+i] = (pdig1[i]<<4)+pdig2[i];
}

void set_pixel(uint8_t r, uint8_t c, uint8_t value)
{
    switch (value)
    {
    case 0: // Clear bit
	display[r] &= (uint8_t) ~(0x80 >> c);
	break;
    case 1: // Set bit
	display[r] |= (0x80 >> c);
	break;
    default: // XOR bit
	display[r] ^= (0x80 >> c);
	break;
    }
}

void rotate_display()
{
	uint8_t display_t[8];
	uint8_t bit = 8;
	uint8_t i,j;
	for (i=0; i<8; i++, bit--)
	{
		display_t[i]=0;
		for (j=0; j<8; j++)
			display_t[i] += (((display[7-j] & (1<<(bit-1))) >> (bit-1))<<(7-j));
	}

	for (i=0; i<8; i++)
		display[i] = display_t[i];
}

void display_temp_humid(unsigned char h, unsigned char temp, unsigned char humid)
{
	image2(h,temp,humid);
	rotate_display();
	update_display();
}

/*
int main(void)
{
    uint8_t i;
    
    max7219_init();
    
	image2('t',1,6);
	rotate_display();
	update_display();
    while(1)
    {
		;
	}
}
*/
