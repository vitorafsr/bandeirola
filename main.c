/*
 * Bandeirola: software para indicar a cor da bandeirola por meio da cor do LED.
 * Pra melhor clareza na indica��o, o display LED 8x8 exibe os valores lidos
 * de temperatura e umidade.
 *
 * Created: 6/23/2017 11:41:31 PM
 * Author : Vitor Augusto Ferreira Santa Rita
 */ 
#define F_CPU 16000000UL

#include "uart.h"
#include "max7219led8x8.h"
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <inttypes.h>
#include "DHT.h"

#define GREEN_PIN 0
#define YELLOW_PIN 1
#define RED_PIN 2
#define BLACK_PIN 3

#define GREEN_COLOR GREEN_PIN
#define YELLOW_COLOR YELLOW_PIN
#define RED_COLOR RED_PIN
#define BLACK_COLOR BLACK_PIN

uint8_t bc[18][4] = \
	{{84,100,0,0},\
	{77,100,0,0},\
	{70,100,0,0},\
	{64,100,0,0},\
	{58,85,100,0},\
	{53,80,100,0},\
	{49,72,86,100},\
	{44,67,79,100},\
	{40,61,73,100},\
	{37,57,68,100},\
	{33,52,63,100},\
	{30,48,58,100},\
	{28,44,54,100},\
	{25,41,50,100},\
	{0,38,46,100},\
	{0,35,46,100},\
	{0,32,39,100},\
	{0,0,0,100}};

void turnOffLeds()
{
	CLEAR_BIT(PORTC,GREEN_PIN);
	CLEAR_BIT(PORTC,YELLOW_PIN);
	CLEAR_BIT(PORTC,RED_PIN);
	CLEAR_BIT(PORTC,BLACK_PIN);
}

void blinkRed()
{
	turnOffLeds();
	while (1)
	{
		SET_BIT(PORTC,RED_PIN);
		_delay_ms(500);
		CLEAR_BIT(PORTC,RED_PIN);
		_delay_ms(500);
	}
}

uint8_t tempDHT(uint8_t* data)
{
	return data[2];
}

uint8_t humidDHT(uint8_t * data)
{
	return data[0];
}

uint8_t bandColor(uint8_t temp, uint8_t humid)
{
	if (temp <= 22)
		return GREEN_COLOR;

	if (temp <= 36)
	{
		int8_t i = GREEN_COLOR;
		while (humid > bc[temp-22-1][i])
			i++;

		return i;
	}
	
	if (temp <= 39)
	{
		int8_t i = YELLOW_COLOR;
		while (humid > bc[temp-22-1][i])
			i++;
		
		return i;
	}

	return BLACK_COLOR;
}

void turnOnLed(uint8_t color)
{
	turnOffLeds();

	switch (color)
	{
		case 0:
		SET_BIT(PORTC,GREEN_PIN);
		break;
		case 1:
		SET_BIT(PORTC,YELLOW_PIN);
		break;
		case 2:
		SET_BIT(PORTC,RED_PIN);
		break;
		case 3:
		SET_BIT(PORTC,BLACK_PIN);
		break;
	}	
}

void writeTempDisplay(unsigned char temp)
{
	unsigned char r;
	r = temp%10;
	display_temp_humid('t', (temp-r)/10, r);	
}

void writeHumidDisplay(unsigned char humid)
{
	unsigned char r;
	r = humid%10;
	display_temp_humid('h', (humid-r)/10, r);	
}


int main(void)
{
	uint8_t data[4];
	unsigned char ret,i;
	ret = 0;

	initUART();

	// define leds ports as output
	SET_BIT(DDRC,GREEN_PIN);
	SET_BIT(DDRC,YELLOW_PIN);
	SET_BIT(DDRC,RED_PIN);
	SET_BIT(DDRC,BLACK_PIN);

	initDHT(); // already waits 2 seconds for sensor stabilization

	max7219_init();

	while (1)
	{
		ret = readDHT(data);
	
		if (ret != 1)
		{
			putByte(ret);
			continue;
		}

		uint8_t temp = tempDHT(data);
		uint8_t humid = humidDHT(data);

		writeString("temp:");
		putValue(temp);
		writeString(", umid:");
		putValue(humid);
		writeString(" ");

		uint8_t color = bandColor(temp, humid);
	
		turnOnLed(color);
	
		writeTempDisplay(temp);
	
		_delay_ms(2000);
	
		writeHumidDisplay(humid);
	
		_delay_ms(2000);
	}
}
