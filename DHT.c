/*
The MIT License (MIT)

Copyright (c) 2017 Vitor Augusto <vitorafsr@gmail.com>
	Adapted to use with 16 bits counter at 16MHz

Copyright (c) 2014 Peter Goldsborough

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "DHT.h"

#include <avr/io.h>
#include <util/delay.h>

void initDHT(void)
{
    /* According to the DHT11's datasheet, the sensor needs about
       1-2 seconds to get ready when first getting power, so we
       wait
     */
    _delay_ms(2000);
}


uint8_t readDHT(uint8_t* arr)
{
    uint8_t data [5];
    uint8_t cnt, check;
    int8_t i,j;
    
    /******************* Sensor communication start *******************/
    
    /* Set data pin as output first */
    SET_BIT(DHT_DDR,DHT_PIN);
    
    /* First we need milliseconds delay, so set clk/1024 prescaler */
    TCCR1B = 0x05;
    
    TCNT1 = 0;
    
    /* Clear bit for 20 ms */
    CLEAR_BIT(DHT_PORT_OUT,DHT_PIN);
    
    /* Wait about 20 ms */
    while(TCNT1 < 320);
    
    /* Now set Timer1 with clk/8 prescaling.
     Gives 0.5µs per cycle at 16Mhz */
    TCCR1B = 0x02;
    
    /* Pull high again */
    SET_BIT(DHT_PORT_OUT,DHT_PIN);
    
    /* And set data pin as input */
    CLEAR_BIT(DHT_DDR,DHT_PIN);
    
    TCNT1 = 0;
    
    /* Wait for response from sensor, 20-40µs according to datasheet */
    while(IS_SET(DHT_PORT_IN,DHT_PIN))
    { if (TCNT1 >= 80) return 'a'; }
    
    /************************* Sensor preamble *************************/
    
    TCNT1 = 0;
    
    /* Now wait for the first response to finish, low ~80µs */
    while(!IS_SET(DHT_PORT_IN,DHT_PIN))
    { if (TCNT1 >= 180) return 'b'; }
    
    TCNT1 = 0;
    
    /* Then wait for the second response to finish, high ~80µs */
    while(IS_SET(DHT_PORT_IN,DHT_PIN))
    { if (TCNT1 >= 180) return 'c'; }
    
    /********************* Data transmission start **********************/
    
    for (i = 0; i < 5; ++i)
    {
        for(j = 7; j >= 0; --j)
        {
            TCNT1 = 0;
            
            /* First there is always a 50µs low period */
            while(!IS_SET(DHT_PORT_IN,DHT_PIN))
            { if (TCNT1 >= 120) return 'd'; }
            
            TCNT1 = 0;
            
            /* Then the data signal is sent. 26 to 28µs (ideally)
             indicate a low bit, and around 70µs a high bit */
            while(IS_SET(DHT_PORT_IN,DHT_PIN))
            { if (TCNT1 >= 160) return 'e'; }
            
            /* Store the value now so that the whole checking doesn't
             move the TCNT1 forward by too much to make the data look
             bad */
            cnt = TCNT1;
            
            if (cnt >= 40 && cnt <= 70)
            { CLEAR_BIT(data[i],j); }
            
            else if (cnt >= 100 && cnt <= 180)
            { SET_BIT(data[i],j); }
            
            else return 'f';
        }
    }
    
    /********************* Sensor communication end *********************/
    
    check = (data[0] + data[1] + data[2] + data[3]) & 0xFF;
    
    if (check != data[4]) return 'g';
    
    for(i = 0; i < 4; ++i)
    { arr[i] = data[i]; }
    
    return 1;
}
