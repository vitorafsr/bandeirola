/*
 * uart.c
 *
 * Asynchronous UART example tested on ATMega328P (16 MHz)
 *
 * Toolchain: avr-gcc (4.3.3)
 * Editor: Eclipse Kepler (4)
 * Usage:
 * 		Perform all settings in uart.h and enable by calling initUART(void)
 * 		Compile:
 * 				make all
 *
 * Functions:
 * 		- First call initUART() to set up Baud rate and frame format
 *		- initUART() calls macros TX_START() and RX_START() automatically
 *		- To enable interrupts on reception, call RX_INTEN() macros
 *		- Call functions getByte() and putByte(char) for character I/O
 *		- Call functions writeString(char*) and readString() for string I/O
 *
 *  Created on: 21-Jan-2014
 *      Author: Shrikant Giridhar
 */

#include "uart.h"

// Debug Mode; comment out on Release
#define _DEBUG			0


/*! \brief Configures baud rate (refer datasheet) */
void initUART(void)
{
	// Not necessary; initialize anyway
	DDRD |= _BV(PD1);
	DDRD &= ~_BV(PD0);

	// Set baud rate; lower byte and top nibble
	UBRR0H = ((_UBRR) & 0xF00);
	UBRR0L = (uint8_t) ((_UBRR) & 0xFF);

	TX_START();
	RX_START();

	// Set frame format = 8-N-1
	UCSR0C = (_DATA << UCSZ00);

}

/*! \brief Returns a byte from the serial buffer
 * 	Use this function if the RX interrupt is not enabled.
 * 	Returns 0 on empty buffer
 */
uint8_t getByte(void)
{
	// Check to see if something was received
	while (!(UCSR0A & _BV(RXC0)));
	return (uint8_t) UDR0;
}


/*! \brief Transmits a byte
 * 	Use this function if the TX interrupt is not enabled.
 * 	Blocks the serial port while TX completes
 */
void putByte(unsigned char data)
{
	// Stay here until data buffer is empty
	while (!(UCSR0A & _BV(UDRE0)));
	UDR0 = (unsigned char) data;

}

void putValue(unsigned char data)
{
	unsigned char i;
	unsigned char u = data%10;
	unsigned char d = (data-u)/10;

	switch(d)
	{
		case 0: putByte('0'); break;
		case 1: putByte('1'); break;
		case 2: putByte('2'); break;
		case 3: putByte('3'); break;
		case 4: putByte('4'); break;
		case 5: putByte('5'); break;
		case 6: putByte('6'); break;
		case 7: putByte('7'); break;
		case 8: putByte('8'); break;
		case 9: putByte('9'); break;
	}
	switch(u)
	{
		case 0: putByte('0'); break;
		case 1: putByte('1'); break;
		case 2: putByte('2'); break;
		case 3: putByte('3'); break;
		case 4: putByte('4'); break;
		case 5: putByte('5'); break;
		case 6: putByte('6'); break;
		case 7: putByte('7'); break;
		case 8: putByte('8'); break;
		case 9: putByte('9'); break;
	}

}

/*! \brief Writes an ASCII string to the TX buffer */
void writeString(char *str)
{
	while (*str != '\0')
	{
		putByte(*str);
		++str;
	}
}

const char* readString(void)
{
	static char rxstr[RX_BUFF];
	static char* temp;
	temp = rxstr;

	while((*temp = getByte()) != '\n')
	{
		++temp;
	}

	return rxstr;
}

#if _DEBUG

	int main(void)
	{
		initUART();
		while(1)
		{
			//writeString(readString());
			putByte('a');
			putByte('l');
			putByte('o');
			putByte('\r');
			putByte('\n');
		}
		return 0;
	}

#endif
