# Bandeirola #

Este é um projeto de um sistema de hardware e software para indicar a cor da bandeirola.

A cor da bandeirola é uma forma de caracterizar a condição climática para a prática de Treinamento Físico Militar (TFM) no Exército Brasileiro.

Segundo o manual de campanha de TFM (EB20-MC-10.350), a cor da bandeirola é obtida pela medida de temperatura e umidade, e pode ser verde, amarela, vermelha ou preta. Quanto mais próxima da cor verde, melhor a condição climática para a prática de TFM. Quanto mais próxima da cor preta, mais adverso está o clima para a prática de TFM.

O circuito com microcontrolador faz a leitura da temperatura e da umidade (via sensor), e informa a cor da bandeirola por meio de um LED na cor da bandeirola, e indica no display 8x8 os valores de temperatura e umidade medidos.

Componentes utilizados:

* Kit Arduino ATmega328P + Bootloader
* Fonte ajustável para protoboard
* Protoboard 830 pontos MB-102
* Fonte bivolt 12V / 1A
* Módulo Matriz de LED 8x8 com MAX7219
* Sensor de Umidade e Temperatura DHT11
* Kit de Leds

O software foi desenvolvido em AVR C.

Um video do funcionamento pode ser visto em: https://youtu.be/B2i2BFvEzNI

Esquematico e PCB estao em: https://easyeda.com/vitorafsr

Blog post em http://registrosdeengenharia.blogspot.com.br/2017/07/o-projeto-bandeirola.html
