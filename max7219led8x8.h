#ifndef _max7219led8x8_h
#define _max7219led8x8_h

void max7219_init();
void display_temp_humid(unsigned char h, unsigned char temp, unsigned char humid);

#endif
