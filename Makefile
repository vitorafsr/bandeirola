all: compile hex flash

compile:
	avr-gcc -Os -mmcu=atmega328p *.c -o bandeirola.elf

hex:
	avr-objcopy -j .text -j .data -O ihex bandeirola.elf bandeirola.hex

flash:
	sudo avrdude -c usbasp -p m328p -B 1 -U flash:w:bandeirola.hex:i -F
